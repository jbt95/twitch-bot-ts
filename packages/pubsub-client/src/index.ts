import * as WebSocket from "ws";

const socket = new WebSocket("ws://localhost:5000");

socket.on("open", () => {
  socket.send(
    JSON.stringify({
      action: "SUBSCRIBE",
      topic: "COMMANDS",
      msg: "xdd",
      issuerId: "203923",
    })
  );
});

socket.on("ping", s => {
  console.log(s.toString());
});

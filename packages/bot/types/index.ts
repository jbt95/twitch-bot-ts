export enum MsgTypes {
  PRIVMSG = 'PRIVMSG',
  ROOMSTATE = 'ROOMSTATE',
  CLEARCHAT = 'CLEARCHAT',
  CLEARMSG = 'CLEARMSG',
  GLOBALUSERSTATE = 'GLOBALUSERSTATE',
  USERNOTICE = 'USERNOTICE',
  USERSTATE = 'USERSTATE',
  NOTICE = 'NOTICE',
  HOSTTARGET = 'HOSTTARGET',
}

export interface PrivMsg {
  tags: PrivMsgTags;
  user: string;
  msgType: MsgTypes;
  channel: string;
  msgRaw: string;
}

export type IrcEventType =
  | 'onPrivMsg'
  | 'onRoomStateMsg'
  | 'onClearChatMsg'
  | 'onClearMsg'
  | 'onGlobalUserStateMsg'
  | 'onUserNoticeMsg'
  | 'onUserStateMsg'
  | 'onNoticeMsg'
  | 'onHostTargetMsg';

export interface ClearChatTags {
  banDuration: string;
}

export interface ClearMsgTags {
  login: string;
  message: string;
  targetMsgId: string;
}

export interface GlobalUserStateTags {
  badgeInfo: string;
  badges: string;
  color: string;
  displayName: string;
  emoteSets: string;
  userId: string;
}

export interface PrivMsgTags {
  badgeInfo: string;
  badges: string;
  bits: string;
  color: string;
  displayName: string;
  emotes: string;
  id: string;
  message: string;
  mod: boolean;
  roomId: string;
  tmiSentTs: string;
  userId: string;
  userType: string;
}

export interface RoomStateTags {
  emoteOnly: boolean;
  followersOnly: boolean;
  r9k: boolean;
  slow: string;
  subsOnly: boolean;
}

export interface UserNoticeTags {
  badgeInfo: string;
  badges: string;
  color: string;
  displayName: string;
  emotes: string;
  id: string;
  login: string;
  message: string;
  mod: boolean;
  msgId: string;
  roomId: string;
  systemMsg: string;
  tmiSentTs: string;
  userId: string;
  msgParamCumulativeMonths?: string;
  msgParamDisplayName?: string;
  msgParamLogin?: string;
  msgParamMonths?: string;
  msgParamPromoName?: string;
  msgParamRecipientDisplayName?: string;
  msgParamRecipientId?: string;
  msgParamsRecipientUserName?: string;
  msgParamSenderLogin?: string;
  msgParamSenderName?: string;
  msgParamShouldShareStreak?: string;
  msgParamStreakMonths?: string;
  msgParamSubPlan?: string;
  msgParamSubPlanName?: string;
  msgParamViewerCount?: string;
  msgParamRitualName?: string;
  msgParamThreshold?: string;
}

export interface NoticeTags {
  message: string;
  msgId: string;
}

export interface UserStateTags {
  badgeInfo: string;
  badges: string;
  color: string;
  displayName: string;
  emoteSets: string;
  mod: boolean;
}

export interface RoomStateMsg {
  tags: RoomStateTags;
  msgType: MsgTypes;
  channel: string;
}

export interface ClearChatMsg {
  tags: ClearChatTags;
  msgType: MsgTypes;
  channel: string;
  user: string;
}

export interface ClearMsg {
  tags: ClearMsgTags;
  msgType: MsgTypes;
  channel: string;
  msgRaw: string;
}

export interface GlobalUserStateMsg {
  tags: GlobalUserStateTags;
  msgType: MsgTypes;
}

export interface UserNoticeMsg {
  tags: UserNoticeTags;
  msgType: MsgTypes;
  channel: string;
  msgRaw: string;
}

export interface UserStateMsg {
  tags: RoomStateTags;
  msgType: MsgTypes;
  channel: string;
}

export interface NoticeMsg {
  tags: NoticeTags;
  msgType: MsgTypes;
  channel: string;
  msgRaw: string;
}

export interface HostTargetMsg {
  msgType: MsgTypes;
  channel: string;
  hostedChannel: string;
  viewers: string;
}

import * as net from "net";
import {
  IrcEventType,
  PrivMsgTags,
  PrivMsg,
  MsgTypes,
  RoomStateTags,
  RoomStateMsg,
  ClearChatMsg,
  ClearChatTags,
  ClearMsgTags,
  ClearMsg,
  GlobalUserStateMsg,
  GlobalUserStateTags,
  UserNoticeTags,
  UserNoticeMsg,
  UserStateTags,
  UserStateMsg,
  NoticeTags,
  NoticeMsg,
  HostTargetMsg,
} from "../types";
import { EventEmitter } from "events";

export class TwitchIrcService {
  private readonly token: string;
  private readonly host: string;
  private readonly port: number;
  private readonly botName: string;
  private readonly channel: string;
  private client: net.Socket;
  private emitter: EventEmitter;

  constructor(token: string, channel: string, botName: string) {
    this.token = token;
    this.host = "irc.chat.twitch.tv";
    this.port = 6667;
    this.channel = channel;
    this.botName = botName;
    this.emitter = new EventEmitter();
    this.client = net.createConnection(
      { host: this.host, port: this.port },
      () => {
        console.log(`connected to ${this.host}:${this.port}`);
      }
    );
  }

  connect(): void {
    this.client.on("data", data => {
      data
        .toString()
        .split("\r\n")
        .forEach(line => {
          line.split(" ").forEach(msgPart => {
            if (Object.values(MsgTypes).includes(msgPart)) {
              this.handleMsg(msgPart, line);
              return;
            }
          });
        });
    });
    this.setupConnection();
  }

  setupConnection(): void {
    this.client.write("CAP REQ :twitch.tv/tags\r\n");
    this.client.write("CAP REQ :twitch.tv/membership\r\n");
    this.client.write("CAP REQ :twitch.tv/commands\r\n");
    this.client.write(`PASS oauth:${this.token}\r\n`);
    this.client.write(`NICK ${this.botName}\r\n`);
    this.client.write(`JOIN #${this.channel}\r\n`, () => {
      console.log(`joined channel ${this.channel}`);
    });
  }

  handleMsg(msgType: string, line: string): void {
    switch (msgType) {
      case MsgTypes.PRIVMSG:
        this.parsePrivMsg(line);
        break;
      case MsgTypes.CLEARCHAT:
        this.parseClearChatMsg(line);
        break;
      case MsgTypes.CLEARMSG:
        this.parseClearMsg(line);
        break;
      case MsgTypes.GLOBALUSERSTATE:
        this.parseGlobalUserStateMsg(line);
        break;
      case MsgTypes.HOSTTARGET:
        this.parseHostTargetMsg(line);
        break;
      case MsgTypes.NOTICE:
        this.parseNoticeMsg(line);
        break;
      case MsgTypes.ROOMSTATE:
        this.parseRoomStateMsg(line);
        break;
      case MsgTypes.USERNOTICE:
        this.parseUserNoticeMsg(line);
        break;
      case MsgTypes.USERSTATE:
        this.parseUserStateMsg(line);
        break;
      default:
        break;
    }
  }

  parseTags<TTags extends any>(tagsRaw: string): TTags {
    const tagsMap = {} as TTags;
    tagsRaw
      .split(";")
      .map(tag => {
        return tag.split("=");
      })
      .forEach(([key, value]) => {
        if (key.startsWith("@")) {
          key = key.substring(1);
        }
        if (key.search("-") > 0) {
          const keyParts = key.split("-");
          for (let i = 1; i < keyParts.length; i++) {
            keyParts[i] =
              keyParts[i].charAt(0).toUpperCase() + keyParts[i].slice(1);
          }
          const camelCaseKey = keyParts.join("");
          Object.assign(tagsMap, { [camelCaseKey]: value });
        } else {
          Object.assign(tagsMap, { [key]: value });
        }
      });
    return tagsMap;
  }

  parseMsg<TTags extends any, TMsg extends any>(
    line: string,
    msgType: MsgTypes
  ): TMsg {
    const tags = {} as TTags;
    const msg = {} as TMsg;
    if (line.startsWith("@")) {
      const tagsRaw = line.split(" ", 1)[0];
      const parsedTags = this.parseTags<TTags>(tagsRaw);
      Object.assign(tags, parsedTags);
    }
    let buffer = "";
    let parsingMessage = false;
    msg.tags = tags;
    for (let i = line.indexOf(" ", 0) + 2; i < line.length; i++) {
      if (
        (buffer === "tmi.twitch.tv" || buffer.match(/^(\w+)!(\w+)@(\w+)/)) &&
        !parsingMessage &&
        TwitchIrcService.isWhiteSpace(line[i])
      ) {
        buffer = "";
        continue;
      } else if (!parsingMessage) {
        if (buffer === msgType) {
          msg.msgType = msgType;
          buffer = "";
          continue;
        } else if (
          buffer.startsWith("#") &&
          TwitchIrcService.isWhiteSpace(line[i])
        ) {
          msg.channel = buffer.substring(1);
          buffer = "";
          parsingMessage = true;
          continue;
        }
      } else if (
        parsingMessage &&
        buffer.startsWith(":") &&
        i + 1 === line.length
      ) {
        buffer += line[i];
        msg.msgRaw = buffer.substring(1);
        break;
      }
      buffer += line[i];
    }
    return msg;
  }

  parseRoomStateMsg(line: string): void {
    const tags = {} as RoomStateTags;
    if (line.startsWith("@")) {
      const tagsRaw = line.split(" ", 1)[0];
      const parsedTags = this.parseTags<RoomStateTags>(tagsRaw);
      Object.assign(tags, parsedTags);
    }
    this.emitter.emit("onRoomStateMsg", {
      tags,
      msgType: MsgTypes.ROOMSTATE,
      channel: line.split(" ")[3],
    } as RoomStateMsg);
  }

  parseClearChatMsg(line: string): void {
    const tags = {} as ClearChatTags;
    const msgParts = line.split(" ");
    if (line.startsWith("@")) {
      const tagsRaw = msgParts[0];
      const parsedTags = this.parseTags<ClearChatTags>(tagsRaw);
      Object.assign(tags, parsedTags);
    }
    this.emitter.emit("onClearChatMsg", {
      tags,
      msgType: MsgTypes.CLEARCHAT,
      channel: msgParts[3].substring(1),
      user: msgParts[4].substring(1),
    } as ClearChatMsg);
  }

  parseGlobalUserStateMsg(line: string): void {
    const tags = {} as GlobalUserStateTags;
    if (line.startsWith("@")) {
      const tagsRaw = line.split(" ")[0];
      const parsedTags = this.parseTags<GlobalUserStateTags>(tagsRaw);
      Object.assign(tags, parsedTags);
    }
    this.emitter.emit("onGlobalUserStateMsg", {
      tags,
      msgType: MsgTypes.GLOBALUSERSTATE,
    } as GlobalUserStateMsg);
  }

  parseHostTargetMsg(line: string): void {
    const msgParts = line.split(" ");
    this.emitter.emit("onHostTargetMsg", {
      msgType: MsgTypes.HOSTTARGET,
      channel: msgParts[2],
      hostedChannel: msgParts[3].substring(1),
      viewers: msgParts[4],
    } as HostTargetMsg);
  }

  parsePrivMsg(line: string): void {
    const msg = this.parseMsg<PrivMsgTags, PrivMsg>(line, MsgTypes.PRIVMSG);
    this.emitter.emit("onPrivMsg", msg);
  }

  parseClearMsg(line: string): void {
    const msg = this.parseMsg<ClearMsgTags, ClearMsg>(line, MsgTypes.CLEARMSG);
    this.emitter.emit("onClearMsg", msg);
  }

  parseUserNoticeMsg(line: string): void {
    const msg = this.parseMsg<UserNoticeTags, UserNoticeMsg>(
      line,
      MsgTypes.USERNOTICE
    );
    this.emitter.emit("onUserNoticeMsg", msg);
  }

  parseUserStateMsg(line: string): void {
    const msg = this.parseMsg<UserStateTags, UserStateMsg>(
      line,
      MsgTypes.USERSTATE
    );
    this.emitter.emit("onUserStateMsg", msg);
  }

  parseNoticeMsg(line: string): void {
    const msg = this.parseMsg<NoticeTags, NoticeMsg>(line, MsgTypes.NOTICE);
    this.emitter.emit("onNoticeMsg", msg);
  }

  on<TMsg extends any>(event: IrcEventType, cb: (msg: TMsg) => void): void {
    this.emitter.on(event, cb);
  }

  static isWhiteSpace(c: string): boolean {
    return c === " ";
  }
}

import gql from "graphql-tag";
import * as React from "react";
import * as ReactApollo from "react-apollo";
export type Maybe<T> = T | null;
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Command = {
  __typename?: "Command";
  id: Scalars["Float"];
  command: Scalars["String"];
  message: Scalars["String"];
  userLevel: Scalars["String"];
};

export type CommandInput = {
  command: Scalars["String"];
  message: Scalars["String"];
  userLevel: Scalars["String"];
};

export type Mutation = {
  __typename?: "Mutation";
  createCommand: Command;
  updateCommand: Scalars["Boolean"];
  deleteCommand: Scalars["Boolean"];
};

export type MutationCreateCommandArgs = {
  data: CommandInput;
};

export type MutationUpdateCommandArgs = {
  data: UpdateCommandInput;
};

export type MutationDeleteCommandArgs = {
  id: Scalars["Float"];
};

export type Query = {
  __typename?: "Query";
  me?: Maybe<User>;
  getCommands: Array<Command>;
};

export type UpdateCommandInput = {
  command: Scalars["String"];
  message: Scalars["String"];
  userLevel: Scalars["String"];
  id: Scalars["Float"];
};

export type User = {
  __typename?: "User";
  id: Scalars["String"];
  login: Scalars["String"];
  email: Scalars["String"];
  accessToken: Scalars["String"];
  refreshToken: Scalars["String"];
  expiresIn: Scalars["String"];
};
export type MeQueryVariables = {};

export type MeQuery = { __typename?: "Query" } & {
  me: Maybe<{ __typename?: "User" } & Pick<User, "id" | "login" | "email">>;
};

export type GetCommandsQueryVariables = {};

export type GetCommandsQuery = { __typename?: "Query" } & {
  getCommands: Array<
    { __typename?: "Command" } & Pick<
      Command,
      "id" | "command" | "message" | "userLevel"
    >
  >;
};

export type CreateCommandMutationVariables = {
  data: CommandInput;
};

export type CreateCommandMutation = { __typename?: "Mutation" } & {
  createCommand: { __typename?: "Command" } & Pick<
    Command,
    "id" | "command" | "message" | "userLevel"
  >;
};

export type UpdateCommandMutationVariables = {
  data: UpdateCommandInput;
};

export type UpdateCommandMutation = { __typename?: "Mutation" } & Pick<
  Mutation,
  "updateCommand"
>;

export type DeleteCommandMutationVariables = {
  id: Scalars["Float"];
};

export type DeleteCommandMutation = { __typename?: "Mutation" } & Pick<
  Mutation,
  "deleteCommand"
>;

export const MeDocument = gql`
  query me {
    me {
      id
      login
      email
    }
  }
`;
export type MeComponentProps = Omit<
  Omit<ReactApollo.QueryProps<MeQuery, MeQueryVariables>, "query">,
  "variables"
> & { variables?: MeQueryVariables };

export const MeComponent = (props: MeComponentProps) => (
  <ReactApollo.Query<MeQuery, MeQueryVariables> query={MeDocument} {...props} />
);

export type MeProps<TChildProps = {}> = Partial<
  ReactApollo.DataProps<MeQuery, MeQueryVariables>
> &
  TChildProps;
export function withMe<TProps, TChildProps = {}>(
  operationOptions?: ReactApollo.OperationOption<
    TProps,
    MeQuery,
    MeQueryVariables,
    MeProps<TChildProps>
  >
) {
  return ReactApollo.withQuery<
    TProps,
    MeQuery,
    MeQueryVariables,
    MeProps<TChildProps>
  >(MeDocument, {
    alias: "withMe",
    ...operationOptions,
  });
}
export const GetCommandsDocument = gql`
  query getCommands {
    getCommands {
      id
      command
      message
      userLevel
    }
  }
`;
export type GetCommandsComponentProps = Omit<
  Omit<
    ReactApollo.QueryProps<GetCommandsQuery, GetCommandsQueryVariables>,
    "query"
  >,
  "variables"
> & { variables?: GetCommandsQueryVariables };

export const GetCommandsComponent = (props: GetCommandsComponentProps) => (
  <ReactApollo.Query<GetCommandsQuery, GetCommandsQueryVariables>
    query={GetCommandsDocument}
    {...props}
  />
);

export type GetCommandsProps<TChildProps = {}> = Partial<
  ReactApollo.DataProps<GetCommandsQuery, GetCommandsQueryVariables>
> &
  TChildProps;
export function withGetCommands<TProps, TChildProps = {}>(
  operationOptions?: ReactApollo.OperationOption<
    TProps,
    GetCommandsQuery,
    GetCommandsQueryVariables,
    GetCommandsProps<TChildProps>
  >
) {
  return ReactApollo.withQuery<
    TProps,
    GetCommandsQuery,
    GetCommandsQueryVariables,
    GetCommandsProps<TChildProps>
  >(GetCommandsDocument, {
    alias: "withGetCommands",
    ...operationOptions,
  });
}
export const CreateCommandDocument = gql`
  mutation createCommand($data: CommandInput!) {
    createCommand(data: $data) {
      id
      command
      message
      userLevel
    }
  }
`;
export type CreateCommandMutationFn = ReactApollo.MutationFn<
  CreateCommandMutation,
  CreateCommandMutationVariables
>;
export type CreateCommandComponentProps = Omit<
  Omit<
    ReactApollo.MutationProps<
      CreateCommandMutation,
      CreateCommandMutationVariables
    >,
    "mutation"
  >,
  "variables"
> & { variables?: CreateCommandMutationVariables };

export const CreateCommandComponent = (props: CreateCommandComponentProps) => (
  <ReactApollo.Mutation<CreateCommandMutation, CreateCommandMutationVariables>
    mutation={CreateCommandDocument}
    {...props}
  />
);

export type CreateCommandProps<TChildProps = {}> = Partial<
  ReactApollo.MutateProps<CreateCommandMutation, CreateCommandMutationVariables>
> &
  TChildProps;
export function withCreateCommand<TProps, TChildProps = {}>(
  operationOptions?: ReactApollo.OperationOption<
    TProps,
    CreateCommandMutation,
    CreateCommandMutationVariables,
    CreateCommandProps<TChildProps>
  >
) {
  return ReactApollo.withMutation<
    TProps,
    CreateCommandMutation,
    CreateCommandMutationVariables,
    CreateCommandProps<TChildProps>
  >(CreateCommandDocument, {
    alias: "withCreateCommand",
    ...operationOptions,
  });
}
export const UpdateCommandDocument = gql`
  mutation updateCommand($data: UpdateCommandInput!) {
    updateCommand(data: $data)
  }
`;
export type UpdateCommandMutationFn = ReactApollo.MutationFn<
  UpdateCommandMutation,
  UpdateCommandMutationVariables
>;
export type UpdateCommandComponentProps = Omit<
  Omit<
    ReactApollo.MutationProps<
      UpdateCommandMutation,
      UpdateCommandMutationVariables
    >,
    "mutation"
  >,
  "variables"
> & { variables?: UpdateCommandMutationVariables };

export const UpdateCommandComponent = (props: UpdateCommandComponentProps) => (
  <ReactApollo.Mutation<UpdateCommandMutation, UpdateCommandMutationVariables>
    mutation={UpdateCommandDocument}
    {...props}
  />
);

export type UpdateCommandProps<TChildProps = {}> = Partial<
  ReactApollo.MutateProps<UpdateCommandMutation, UpdateCommandMutationVariables>
> &
  TChildProps;
export function withUpdateCommand<TProps, TChildProps = {}>(
  operationOptions?: ReactApollo.OperationOption<
    TProps,
    UpdateCommandMutation,
    UpdateCommandMutationVariables,
    UpdateCommandProps<TChildProps>
  >
) {
  return ReactApollo.withMutation<
    TProps,
    UpdateCommandMutation,
    UpdateCommandMutationVariables,
    UpdateCommandProps<TChildProps>
  >(UpdateCommandDocument, {
    alias: "withUpdateCommand",
    ...operationOptions,
  });
}
export const DeleteCommandDocument = gql`
  mutation deleteCommand($id: Float!) {
    deleteCommand(id: $id)
  }
`;
export type DeleteCommandMutationFn = ReactApollo.MutationFn<
  DeleteCommandMutation,
  DeleteCommandMutationVariables
>;
export type DeleteCommandComponentProps = Omit<
  Omit<
    ReactApollo.MutationProps<
      DeleteCommandMutation,
      DeleteCommandMutationVariables
    >,
    "mutation"
  >,
  "variables"
> & { variables?: DeleteCommandMutationVariables };

export const DeleteCommandComponent = (props: DeleteCommandComponentProps) => (
  <ReactApollo.Mutation<DeleteCommandMutation, DeleteCommandMutationVariables>
    mutation={DeleteCommandDocument}
    {...props}
  />
);

export type DeleteCommandProps<TChildProps = {}> = Partial<
  ReactApollo.MutateProps<DeleteCommandMutation, DeleteCommandMutationVariables>
> &
  TChildProps;
export function withDeleteCommand<TProps, TChildProps = {}>(
  operationOptions?: ReactApollo.OperationOption<
    TProps,
    DeleteCommandMutation,
    DeleteCommandMutationVariables,
    DeleteCommandProps<TChildProps>
  >
) {
  return ReactApollo.withMutation<
    TProps,
    DeleteCommandMutation,
    DeleteCommandMutationVariables,
    DeleteCommandProps<TChildProps>
  >(DeleteCommandDocument, {
    alias: "withDeleteCommand",
    ...operationOptions,
  });
}

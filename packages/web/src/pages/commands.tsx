import React, { useState } from "react";
import Navbar from "../components/Nav";
import {
  Grid,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Table,
  Theme,
  Toolbar,
  Typography,
  Fab,
  withStyles,
  IconButton,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { StyleRulesCallback } from "@material-ui/core/styles/withStyles";
import { RouteComponentProps } from "react-router";
import { withAuth } from "../components/withAuth";
import CommandsModal from "../components/Modal/commandsModal";
import { CreateCommandComponent, GetCommandsComponent } from "../generated";

export interface Command {
  command: string;
  message: string;
  userLevel: string;
}

const styles: StyleRulesCallback<string> = ({
  palette,
  breakpoints,
}: Theme) => ({
  grid: {
    margin: "100px auto",
    [breakpoints.between("xs", "sm")]: {
      margin: "100px auto",
    },
  },
  paper: {
    backgroundColor: palette.primary.dark,
  },
  tooltip: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
});

type Props = RouteComponentProps & { classes: any };

const Commands: React.FC<Props> = ({ classes, ...props }) => {
  const [modalState, setModalState] = useState(false);
  const [commandsState, setCommandsState] = useState([] as Command[]);

  return (
    <React.Fragment>
      <Navbar {...props} selectedKey={{ commands: true }} />
      <Grid container={true}>
        <Grid item={true} xs={10} md={8} className={classes.grid}>
          <Paper className={classes.paper}>
            <Toolbar>
              <div className={classes.tooltip}>
                <Typography variant="h6">Commands</Typography>
                <Fab
                  color="secondary"
                  variant="extended"
                  size="small"
                  onClick={() => setModalState(true)}
                >
                  <AddIcon />
                  <Typography variant="subtitle2">Add command</Typography>
                </Fab>
              </div>
            </Toolbar>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Command</TableCell>
                  <TableCell>Message</TableCell>
                  <TableCell>User level</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <GetCommandsComponent>
                  {({ loading, data, error }) => {
                    if (loading || error || !data || !data.getCommands) {
                      return null;
                    }
                    if (data) {
                      return data.getCommands.map((cmd, i) => {
                        return (
                          <TableRow key={cmd + i.toString()}>
                            <TableCell>{cmd.command}</TableCell>
                            <TableCell>{cmd.message}</TableCell>
                            <TableCell>{cmd.userLevel}</TableCell>
                            <TableCell align="left">
                              <IconButton>
                                <DeleteIcon />
                              </IconButton>
                              <IconButton>
                                <EditIcon />
                              </IconButton>
                            </TableCell>
                          </TableRow>
                        );
                      });
                    }
                  }}
                </GetCommandsComponent>
              </TableBody>
            </Table>
          </Paper>
        </Grid>
      </Grid>
      <CreateCommandComponent>
        {mutate => {
          return (
            <CommandsModal
              visible={modalState}
              onClose={() => setModalState(false)}
              onSubmit={async values => {
                const response = await mutate({ variables: { data: values } });
                if (!response || !response.data) {
                  console.log("failed to create command");
                  return;
                }
                setModalState(false);
                setCommandsState([
                  ...commandsState,
                  response.data.createCommand,
                ]);
              }}
            />
          );
        }}
      </CreateCommandComponent>
      );
    </React.Fragment>
  );
};

export default withStyles(styles)(withAuth(Commands));

import React, { useState, useEffect, useContext } from "react";
import styled from "styled-components";
import { Button, CircularProgress } from "@material-ui/core";
import TwitchLogo from "../static/twitch_svg.svg";
import { RouteComponentProps } from "react-router";
import { ApolloContext } from "../components/Context/apolloContext";
import { meQuery } from "../graphql/auth";

const MyContainer = styled("div")`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const Home: React.FC<RouteComponentProps> = ({ history }) => {
  const [loading, setLoading] = useState(true);
  const apollo = useContext(ApolloContext);

  const authRequest = async () => {
    const { data } = await apollo.query({
      query: meQuery,
    });
    if (data && data.me) {
      history.push("/dashboard");
      return;
    }
    setLoading(false);
  };

  useEffect(() => {
    authRequest();
  });

  return (
    <MyContainer>
      {loading ? (
        <CircularProgress color="secondary" />
      ) : (
        <Button
          href="http://localhost:4000/oauth/twitchtv"
          variant="contained"
          color="secondary"
          style={{ fontSize: "18px" }}
        >
          <img src={TwitchLogo} width="30px" alt="twitch logo" />
          Connect with twitch
        </Button>
      )}
    </MyContainer>
  );
};

import React from "react";
import Navbar from "../components/Nav";
import { RouteComponentProps } from "react-router";
import { withAuth } from "../components/withAuth";
import {
  Grid,
  StyleRulesCallback,
  Theme,
  withStyles,
  ListItemText,
  List,
  ListItem,
  Typography,
  Divider,
  IconButton,
  Button,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

const styles: StyleRulesCallback<string> = ({
  palette,
  breakpoints,
}: Theme) => ({
  videoContainer: {
    backgroundColor: palette.primary.dark,
    margin: "50px 0  50px 150px",
    height: "500px",
    [breakpoints.down("sm")]: {
      margin: "100px auto",
      height: "250px",
    },
  },
  songsContainer: {
    margin: "50px 0  50px 150px",
    [breakpoints.down("sm")]: {
      margin: "30px auto",
    },
  },
  listItem: {
    backgroundColor: palette.primary.dark,
  },
});

interface Props extends RouteComponentProps {
  classes: any;
}

const SongRequest: React.FC<Props> = props => {
  return (
    <React.Fragment>
      <Navbar {...props} selectedKey={{ songRequest: true }} />
      <Grid container={true}>
        <Grid
          item={true}
          className={props.classes.videoContainer}
          xs={8}
          md={4}
        />
        <Grid
          item={true}
          className={props.classes.songsContainer}
          xs={8}
          md={4}
        >
          <List
            component="nav"
            subheader={<Typography variant="h6">Songs</Typography>}
          >
            <ListItem className={props.classes.listItem}>
              <ListItemText primary="Song blabla" />
              <Button color="secondary" variant="contained">
                PLAY
              </Button>
              <IconButton>
                <DeleteIcon />
              </IconButton>
            </ListItem>
            <Divider />
          </List>
        </Grid>
      </Grid>
    </React.Fragment>
  );
};

export default withStyles(styles)(withAuth(SongRequest));

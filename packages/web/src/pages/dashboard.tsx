import React from "react";
import NavBar from "../components/Nav";
import { RouteComponentProps } from "react-router";
import { withAuth } from "../components/withAuth";

const Dashboard: React.FC<RouteComponentProps> = props => <NavBar {...props} />;

export default withAuth(Dashboard);

import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import * as serviceWorker from "./serviceWorker";
import { Home } from "./pages";
import Dashboard from "./pages/dashboard";
import Commands from "./pages/commands";
import {
  createMuiTheme,
  MuiThemeProvider,
  CssBaseline,
} from "@material-ui/core";
import SongRequest from "./pages/songRequest";
import { ApolloClient, InMemoryCache } from "apollo-boost";
import { createHttpLink } from "apollo-link-http";
import { ApolloProvider } from "react-apollo";
import { ApolloContext } from "./components/Context/apolloContext";

const link = createHttpLink({
  uri: "http://localhost:4000/graphql",
  credentials: "include",
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link,
});

const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: "#37474F",
      dark: "#102027",
      light: "#62727b",
      contrastText: "#ffffff",
    },
    secondary: {
      main: "#546E7A",
      dark: "#29434e",
      light: "#819ca9",
      contrastText: "#ffffff",
    },
    background: {
      default: "#37474F",
    },
  },
  typography: {
    useNextVariants: true,
  },
});

ReactDOM.render(
  <ApolloContext.Provider value={client}>
    <ApolloProvider client={client}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <BrowserRouter>
          <Route path="/" exact={true} component={Home} />
          <Route path="/dashboard" exact={true} component={Dashboard} />
          <Route path="/commands" exact={true} component={Commands} />
          <Route path="/song-request" exact={true} component={SongRequest} />
        </BrowserRouter>
      </MuiThemeProvider>
    </ApolloProvider>
  </ApolloContext.Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();

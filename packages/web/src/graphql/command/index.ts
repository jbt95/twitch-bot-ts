import gql from "graphql-tag";

export const getCommandsQuery = gql`
  query getCommands {
    getCommands {
      id
      command
      message
      userLevel
    }
  }
`;

export const createCommandMutation = gql`
  mutation createCommand($data: CommandInput!) {
    createCommand(data: $data) {
      id
      command
      message
      userLevel
    }
  }
`;

export const updateCommandMutation = gql`
  mutation updateCommand($data: UpdateCommandInput!) {
    updateCommand(data: $data)
  }
`;

export const deleteCommandMutation = gql`
  mutation deleteCommand($id: Float!) {
    deleteCommand(id: $id)
  }
`;

import React from "react";
import { CircularProgress } from "@material-ui/core";
import styled from "styled-components";
import { ApolloContext } from "./Context/apolloContext";
import { meQuery } from "../graphql/auth/index";

interface State {
  loading: boolean;
}

const MyContainer = styled("div")`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const withAuth = <P extends object>(C: React.ComponentType<P>) => {
  return class WithAuthComponent extends React.Component<P, State> {
    static contextType = ApolloContext;
    context!: React.ContextType<typeof ApolloContext>;

    constructor(props: P) {
      super(props);
      this.state = {
        loading: true,
      };
    }

    async componentWillMount() {
      const { location, history } = this.props as any;
      const { data } = await this.context.query({
        query: meQuery,
      });
      if ((!data || !data.me) && location.pathname !== "/") {
        history.replace("/");
        return;
      }
      this.setState({ loading: false });
    }

    render() {
      return (
        <React.Fragment>
          {this.state.loading ? (
            <MyContainer>
              <CircularProgress color="secondary" />
            </MyContainer>
          ) : (
            <C {...this.props} />
          )}
        </React.Fragment>
      );
    }
  };
};

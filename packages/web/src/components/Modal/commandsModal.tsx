import React from "react";
import { Formik, Form } from "formik";
import {
  Paper,
  Theme,
  withStyles,
  Dialog,
  DialogTitle,
  DialogContent,
  Button,
  TextField,
} from "@material-ui/core";
import { StyleRulesCallback } from "@material-ui/core/styles/withStyles";
import { Command } from "../../pages/commands";

interface Props {
  visible: boolean;
  classes: any;
  onClose: () => void;
  onSubmit: (values: Command) => void;
}

const styles: StyleRulesCallback<string> = ({ palette }: Theme) => ({
  paper: {
    backgroundColor: palette.primary.dark,
  },
  input: {
    margin: "20px 0 20px 0",
  },
});

const CommandsModal: React.FC<Props> = ({
  visible,
  onClose,
  classes,
  onSubmit,
}) => {
  return (
    <Dialog open={visible} onClose={onClose} maxWidth={"sm"} fullWidth={true}>
      <Paper className={classes.paper}>
        <DialogTitle>Create command</DialogTitle>
        <DialogContent>
          <Formik
            initialValues={{ command: "", message: "", userLevel: "" }}
            onSubmit={onSubmit}
          >
            {({ handleBlur, handleChange, values }) => {
              return (
                <Form autoComplete="off">
                  <TextField
                    className={classes.input}
                    id="command"
                    value={values.command}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    fullWidth={true}
                    label="Command"
                    autoFocus={true}
                  />
                  <TextField
                    className={classes.input}
                    id="message"
                    value={values.message}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    fullWidth={true}
                    label="Command message"
                  />
                  <TextField
                    className={classes.input}
                    id="userLevel"
                    value={values.userLevel}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    fullWidth={true}
                    label="User level"
                  />
                  <Button type="submit" variant="contained" color="secondary">
                    Create
                  </Button>
                </Form>
              );
            }}
          </Formik>
        </DialogContent>
      </Paper>
    </Dialog>
  );
};

export default withStyles(styles)(CommandsModal);

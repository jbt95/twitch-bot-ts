import { createContext } from "react";
import { ApolloClient, NormalizedCacheObject } from "apollo-boost";

export const ApolloContext = createContext<ApolloClient<NormalizedCacheObject>>(
  {} as ApolloClient<NormalizedCacheObject>
);

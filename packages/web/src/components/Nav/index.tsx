import React, { useState } from "react";
import styled from "styled-components";
import {
  List,
  ListItem,
  ListItemIcon,
  Theme,
  withTheme,
  Typography,
  Toolbar,
  IconButton,
  Drawer,
  Paper,
} from "@material-ui/core";
import MusicNoteIcon from "@material-ui/icons/MusicNote";
import QuestionAnswerIcon from "@material-ui/icons/QuestionAnswer";
import BlockIcon from "@material-ui/icons/Block";
import StorageIcon from "@material-ui/icons/Storage";
import { RouteComponentProps } from "react-router";
import MenuIcon from "@material-ui/icons/Menu";

interface Props extends RouteComponentProps {
  theme: Theme;
  selectedKey?: Partial<{
    commands: boolean;
    songRequest: boolean;
    bannedUsers: boolean;
    logs: boolean;
  }>;
}

interface TooltipProps {
  top: number;
  left: number;
  height: number;
  theme: Theme;
}

const SidebarContainer = styled("div")<{ theme: Theme }>`
  top: 0;
  left: 0;
  position: fixed;
  width: auto;
  height: 100%;
  background-color: ${({ theme }) => theme.palette.primary.dark};
  border-right: 1px solid gray;

  ${({ theme }) => theme.breakpoints.down("sm")} {
    display: none;
  }
`;

const SidebarTooltip = styled("div")<TooltipProps>`
  position: absolute;
  top: ${({ top, height }) => top - 15 - height / 2}px;
  left: ${({ left }) => left + 5}px;
  background-color: ${({ theme }) => theme.palette.primary.dark};
  text-align: center;
  border-radius: 6px;
  color: white;
  font-size: 18px;
  font-family: "Roboto", sans-serif;
  width: auto;
  padding: 6px;
  transition: 0.2s ease-out;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);
`;

const TopBarContainer = styled("div")<{ theme: Theme }>`
  top: 0;
  left: 0;
  width: 100%;
  height: auto;
  background-color: ${({ theme }) => theme.palette.primary.dark};
  position: fixed;
  display: none;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.25);

  ${({ theme }) => theme.breakpoints.down("sm")} {
    display: block;
  }
`;

const NavBar: React.FC<Props> = ({ theme, history, selectedKey }) => {
  const [tooltipState, setTooltipState] = useState({
    currentListElPos: {} as any,
    tooltipVisible: false,
    currentTooltipValue: "",
  });
  const [drawerState, setDrawerState] = useState({
    showListMenuText: false,
    drawerVisible: false,
  });

  const hideTooltip = () => {
    setTooltipState({ ...tooltipState, tooltipVisible: false });
  };

  const handleOnMouseEnter = (
    e: React.MouseEvent<HTMLElement, MouseEvent>,
    tooltipValue: string
  ) => {
    const elPos = e.currentTarget.getBoundingClientRect();
    setTooltipState({
      tooltipVisible: true,
      currentListElPos: elPos,
      currentTooltipValue: tooltipValue,
    });
  };

  const renderList = () => {
    return (
      <List>
        <ListItem
          button={true}
          selected={selectedKey !== undefined ? selectedKey.commands : false}
          onClick={() => history.push("/commands")}
          onMouseEnter={e => handleOnMouseEnter(e, "Commands")}
          onMouseLeave={hideTooltip}
        >
          <ListItemIcon>
            <QuestionAnswerIcon />
          </ListItemIcon>
          {drawerState.showListMenuText ? (
            <Typography variant="h6">Commands</Typography>
          ) : null}
        </ListItem>
        <ListItem
          button={true}
          selected={selectedKey !== undefined ? selectedKey.songRequest : false}
          onClick={() => history.push("/song-request")}
          onMouseEnter={e => handleOnMouseEnter(e, "Song request")}
          onMouseLeave={hideTooltip}
        >
          <ListItemIcon>
            <MusicNoteIcon />
          </ListItemIcon>
          {drawerState.showListMenuText ? (
            <Typography variant="h6">Song request</Typography>
          ) : null}
        </ListItem>
        <ListItem
          button={true}
          selected={selectedKey !== undefined ? selectedKey.bannedUsers : false}
          onMouseEnter={e => handleOnMouseEnter(e, "Banned users")}
          onMouseLeave={hideTooltip}
        >
          <ListItemIcon>
            <BlockIcon />
          </ListItemIcon>
          {drawerState.showListMenuText ? (
            <Typography variant="h6">Banned users</Typography>
          ) : null}
        </ListItem>
        <ListItem
          button={true}
          selected={selectedKey !== undefined ? selectedKey.logs : false}
          onMouseEnter={e => handleOnMouseEnter(e, "Logs")}
          onMouseLeave={hideTooltip}
        >
          <ListItemIcon>
            <StorageIcon />
          </ListItemIcon>
          {drawerState.showListMenuText ? (
            <Typography variant="h6">Logs</Typography>
          ) : null}
        </ListItem>
      </List>
    );
  };

  return (
    <div>
      <SidebarContainer theme={theme}>{renderList()}</SidebarContainer>
      <TopBarContainer theme={theme}>
        <Toolbar>
          <IconButton
            onClick={() =>
              setDrawerState({
                showListMenuText: true,
                drawerVisible: true,
              })
            }
          >
            <MenuIcon />
          </IconButton>
          <Drawer
            anchor="left"
            open={drawerState.drawerVisible}
            onClose={() =>
              setDrawerState({
                showListMenuText: false,
                drawerVisible: false,
              })
            }
          >
            <Paper
              style={{
                backgroundColor: theme.palette.primary.dark,
                height: "100%",
              }}
            >
              {renderList()}
            </Paper>
          </Drawer>
        </Toolbar>
      </TopBarContainer>

      {tooltipState.tooltipVisible && !drawerState.showListMenuText ? (
        <SidebarTooltip
          top={tooltipState.currentListElPos.bottom}
          left={tooltipState.currentListElPos.right}
          theme={theme}
          height={tooltipState.currentListElPos.height}
        >
          {tooltipState.currentTooltipValue}
        </SidebarTooltip>
      ) : null}
    </div>
  );
};

export default withTheme()(NavBar);

import { Entity, Column, PrimaryColumn, OneToMany } from "typeorm";
import { Command } from "./command";
import { ObjectType, Field } from "type-graphql";

@Entity()
@ObjectType()
export class User {
  @PrimaryColumn()
  @Field()
  id: string;

  @Column({ nullable: false })
  @Field()
  login: string;

  @Column({ nullable: false })
  @Field()
  email: string;

  @Column({ nullable: false })
  @Field()
  accessToken: string;

  @Column({ nullable: false })
  @Field()
  refreshToken: string;

  @Column({ nullable: false })
  @Field()
  expiresIn: string;

  @OneToMany(() => Command, command => command.user, { cascade: true })
  commands: Command[];
}

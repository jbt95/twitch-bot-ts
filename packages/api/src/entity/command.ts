import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { User } from "./user";
import { ObjectType, Field } from "type-graphql";

@Entity()
@ObjectType()
export class Command {
  @PrimaryGeneratedColumn()
  @Field()
  id: number;

  @Column({ nullable: false })
  @Field()
  command: string;

  @Column({ nullable: false })
  @Field()
  message: string;

  @Column({ nullable: false })
  @Field()
  userLevel: string;

  @ManyToOne(() => User, user => user.commands)
  user: User;
}

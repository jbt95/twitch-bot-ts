import Container from "typedi";
import { buildSchema } from "type-graphql";
import { AuthResolver } from "../modules/auth/resolver";
import { CommandResolver } from "../modules/commands/resolver";

export function createSchema() {
  return buildSchema({
    resolvers: [AuthResolver, CommandResolver],
    validate: false,
    container: Container,
  });
}

import * as moment from "moment";
import { getConnection } from "typeorm";
import { User } from "../entity/user";
import * as querystring from "querystring";

export function newExpirationDate() {
  return moment()
    .add(1, "hour")
    .unix()
    .toString();
}

export function isExpired(expiresIn: string | null): boolean {
  if (expiresIn) {
    return parseInt(expiresIn, 10) < moment().unix();
  }
  return false;
}

export async function getUserToken(userId: string) {
  const userRepo = getConnection().getRepository(User);
  const user = await userRepo.findOne({
    where: { id: userId },
  });
  if (!user) {
    return null;
  }
  const { accessToken, refreshToken, expiresIn } = user;
  if (!accessToken) {
    return null;
  }
  const expired = isExpired(expiresIn);
  if (accessToken && expired) {
    const url =
      "https://id.twitch.tv/oauth2/token?" +
      querystring.stringify({
        grant_type: "refresh_token",
        refreshToken,
        client_id: process.env.TWITCHTV_CLIENT_ID,
        client_secret: process.env.TWITCHTV_CLIENT_SECRET,
      });
    const response = await fetch(url, { method: "POST" });
    const { access_token } = await response.json();
    user.accessToken = access_token;
    user.expiresIn = newExpirationDate();
    await userRepo.save(user);
    return access_token as string;
  }
  return accessToken;
}

import "reflect-metadata";
import * as session from "express-session";
import * as cors from "cors";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as querystring from "querystring";
import * as TypeOrm from "typeorm";
import fetch from "node-fetch";
import { ApolloServer } from "apollo-server-express";
import { GraphQLResponse } from "graphql-extensions";
import { ResolverContext } from "./types/apolloContext";
import { createSchema } from "./utils/createSchema";
import { createTypeormConn } from "./utils/createTypeormConn";
import { Container } from "typedi";
import { newExpirationDate } from "./utils/auth";
import { User } from "./entity/user";
require("dotenv").config();

const initServer = async () => {
  try {
    const app = express();

    TypeOrm.useContainer(Container);

    const conn = await createTypeormConn();

    const server = new ApolloServer({
      schema: await createSchema(),
      formatError: error => ({
        message: error.message,
        locations: error.locations,
        path: error.path,
      }),
      context: ({ req, res }: any): ResolverContext => {
        return {
          res: res,
          req: req,
        };
      },
      formatResponse: (response: GraphQLResponse) => {
        console.log(response.data);
        return response;
      },
    });

    app.use(
      cors({
        credentials: true,
        origin: "http://localhost:3000",
      })
    );

    app.use(
      session({
        name: "sid",
        secret: "skdjsikdjsidjspd",
        resave: false,
        saveUninitialized: false,
        cookie: {
          httpOnly: true,
          secure: false,
          maxAge: 1000 * 60 * 60 * 24 * 7 * 365,
        },
      })
    );

    app.use(cookieParser());
    app.use(bodyParser.json());

    app.get("/oauth/twitchtv", (_, res) => {
      res.redirect(
        "https://id.twitch.tv/oauth2/authorize?" +
          querystring.stringify({
            client_id: process.env.TWITCHTV_CLIENT_ID,
            redirect_uri: "http://localhost:4000/oauth/twitchtv/callback",
            response_type: "code",
            scope:
              "user:read:email user:edit channel:moderate chat:edit chat:read whispers:read whispers:edit",
          })
      );
    });

    app.get("/oauth/twitchtv/callback", async (req, res) => {
      if (req.query) {
        const { code } = req.query;
        const url =
          "https://id.twitch.tv/oauth2/token?" +
          querystring.stringify({
            client_id: process.env.TWITCHTV_CLIENT_ID,
            client_secret: process.env.TWITCHTV_CLIENT_SECRET,
            code,
            grant_type: "authorization_code",
            redirect_uri: "http://localhost:4000/oauth/twitchtv/callback",
          });
        const twitchOauthResponse = await fetch(url, { method: "POST" });
        const {
          access_token,
          refresh_token,
        } = await twitchOauthResponse.json();
        if (
          access_token &&
          refresh_token &&
          twitchOauthResponse.status === 200
        ) {
          const twitchUserResponse = await fetch(
            "https://api.twitch.tv/helix/users",
            {
              method: "GET",
              headers: {
                Authorization: `Bearer ${access_token}`,
              },
            }
          );
          if (twitchUserResponse.status !== 200) {
            res.status(404).send({ message: "failed get twitch user" });
          }
          const userRepo = conn.getRepository(User);
          const { data } = await twitchUserResponse.json();
          let user = await userRepo.findOne({
            where: {
              id: data[0].id,
            },
          });
          if (!user) {
            user = await userRepo.save({
              email: data[0].email,
              id: data[0].id,
              login: data[0].login,
              accessToken: access_token,
              refreshToken: refresh_token,
              expiresIn: newExpirationDate(),
              commands: [],
            });
          }
          req.session!.user = user;
          res.redirect(`http://localhost:3000/dashboard`);
        } else if (twitchOauthResponse.status !== 200) {
          res.status(400).send({ message: "failed get twitch oauth token" });
        }
      }
    });

    server.applyMiddleware({ app, path: "/graphql", cors: false });

    app.listen({ port: 4000 }, () => {
      console.log(
        `> Server ready at http://localhost:4000${server.graphqlPath}`
      );
    });
  } catch (error) {
    console.log(error);
  }
};

initServer();

import { MiddlewareFn } from "type-graphql";
import { ResolverContext } from "../../types/apolloContext";

export const AuthMiddleware: MiddlewareFn<ResolverContext> = async (
  { context },
  next
) => {
  if (!context.req.session!.user || !context.req.session) {
    throw new Error("no user session found");
  }
  return next();
};

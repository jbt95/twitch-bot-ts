import { InputType, Field } from "type-graphql";

@InputType()
export class CommandInput {
  @Field()
  command: string;

  @Field()
  message: string;

  @Field()
  userLevel: string;
}

@InputType()
export class UpdateCommandInput {
  @Field()
  command: string;

  @Field()
  message: string;

  @Field()
  userLevel: string;

  @Field()
  id: number;
}

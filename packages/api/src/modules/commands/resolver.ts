import {
  Resolver,
  Query,
  Ctx,
  Mutation,
  Arg,
  UseMiddleware,
} from "type-graphql";
import { Repository } from "typeorm";
import { Command } from "../../entity/command";
import { ResolverContext } from "../../types/apolloContext";
import { CommandInput, UpdateCommandInput } from "./input";
import { InjectRepository } from "typeorm-typedi-extensions";
import { AuthMiddleware } from "../middleware/auth";

@Resolver(Command)
export class CommandResolver {
  constructor(
    @InjectRepository(Command)
    private readonly commandsRepo: Repository<Command>
  ) {}

  @Query(() => [Command])
  @UseMiddleware(AuthMiddleware)
  async getCommands(@Ctx() ctx: ResolverContext): Promise<Command[]> {
    const user = ctx.req.session!.user;
    return await this.commandsRepo.find({
      relations: ["user"],
      where: {
        user: {
          id: user.id,
        },
      },
    });
  }

  @Mutation(() => Command)
  @UseMiddleware(AuthMiddleware)
  async createCommand(
    @Ctx() ctx: ResolverContext,
    @Arg("data") data: CommandInput
  ): Promise<Command> {
    const user = ctx.req.session!.user;
    const command = this.commandsRepo.create({
      ...data,
      user,
    });
    return await this.commandsRepo.save(command);
  }

  @Mutation(() => Command)
  @UseMiddleware(AuthMiddleware)
  async updateCommand(@Arg("data") data: UpdateCommandInput) {
    const newCommand = this.commandsRepo.create(data);
    await this.commandsRepo.update({ id: data.id }, newCommand);
    return newCommand;
  }

  @Mutation(() => Boolean)
  @UseMiddleware(AuthMiddleware)
  async deleteCommand(@Arg("id") id: number) {
    const res = await this.commandsRepo.delete({ id });
    return !res.affected || res.affected <= 0 ? false : true;
  }
}

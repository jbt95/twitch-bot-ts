import { Resolver, Ctx, Query } from "type-graphql";
import { ResolverContext } from "../../types/apolloContext";
import { User } from "../../entity/user";

@Resolver(User)
export class AuthResolver {
  @Query(() => User, { nullable: true })
  async me(@Ctx() ctx: ResolverContext) {
    const user = ctx.req.session!.user;
    return !user || !ctx.req.session ? null : (ctx.req.session.user as User);
  }
}
